# Puppet module dependencies
# Maintainer:	57r4n63r <david.a.rancourt@gmail.com>

echo 'installing puppet..'
apt-get install puppet -y

echo 'installing puppet modules...'
puppet module install puppetlabs-apt
puppet module install puppetlabs-apache
puppet module install puppetlabs-vcsrepo
puppet module install puppetlabs-mysql
