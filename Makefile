#! /bin/bash

basedir = "/root/puppet"

install: dependencies apply

dependencies:
	bash ${basedir}/dependencies.sh
apply:
	puppet apply ${basedir}/manifests

.PHONY=install dependencies apply
