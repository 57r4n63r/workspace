class workspace::tmux{
  package { 'tmux':
    ensure => installed,
  }
  vcsrepo { "/home/$workspace::config::user/$workspace::config::packageFolder/tmux":
    ensure   => present,
    provider => git,
    source   => 'https://gitlab.com/57r4n63r/tmux.git',
    user     => $workspace::config::user,
  }
  file{ "/home/$workspace::config::user/.tmux.conf":
    ensure => link,
    target => "/home/$workspace::config::user/$workspace::config::packageFolder/tmux/.tmux.conf",
  }
  file{ "/home/$workspace::config::user/.tmux.conf.local":
    ensure  => present,
    replace => 'no',
    source => "/home/$workspace::config::user/$workspace::config::packageFolder/tmux/.tmux.conf.local"
  }
}
