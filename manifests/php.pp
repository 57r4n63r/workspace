class workspace::php {
  package { 'php':
    ensure => installed,
  }
  package { 'php-mysql':
    ensure => installed,
  }
  package { 'php-cgi' :
    ensure => installed
  }
  package { 'php-cli' :
      ensure => installed
  }
  package { 'php-dev' :
      ensure => installed
  }
  package { 'php-fpm' :
      ensure => installed
  }
  package { 'php-phpdbg' :
      ensure => installed
  }
  package { 'php-bcmath' :
      ensure => installed
  }
  package { 'php-bz2' :
      ensure => installed
  }
  package { 'php-common' :
      ensure => installed
  }
  package { 'php-curl' :
      ensure => installed
  }
  package { 'php-dba' :
      ensure => installed
  }
  package { 'php-enchant' :
      ensure => installed
  }
  package { 'php-gd' :
      ensure => installed
  }
  package { 'php-gmp' :
      ensure => installed
  }
  package { 'php-imap' :
      ensure => installed
  }
  package { 'php-interbase' :
      ensure => installed
  }
  package { 'php-intl' :
      ensure => installed
  }
  package { 'php-json' :
      ensure => installed
  }
  package { 'php-ldap' :
      ensure => installed
  }
  package { 'php-mbstring' :
      ensure => installed
  }
  package { 'php-odbc' :
      ensure => installed
  }
  package { 'php-pgsql' :
      ensure => installed
  }
  package { 'php-pspell' :
      ensure => installed
  }
  package { 'php-readline' :
      ensure => installed
  }
  package { 'php-recode' :
      ensure => installed
  }
  package { 'php-snmp' :
      ensure => installed
  }
  package { 'php-soap' :
      ensure => installed
  }
  package { 'php-sqlite3' :
      ensure => installed
  }
  package { 'php-sybase' :
      ensure => installed
  }
  package { 'php-tidy' :
      ensure => installed
  }
  package { 'php-xml' :
      ensure => installed
  }
  package { 'php-xmlrpc' :
      ensure => installed
  }
  package { 'php-zip' :
      ensure => installed
  }
  package { 'php-opcache' :
      ensure => installed
  }
  package { 'php-xsl' :
      ensure => installed
  }
}

