class workspace::vscode{
  # dependencie required
  package{ 'libgconf-2-4':
    ensure => present
  }
  exec{'retrieve_vscode_deb':
    command => "/usr/bin/wget --user-agent=Mozilla --content-disposition -E -c https://go.microsoft.com/fwlink/?LinkID=760865 --no-check-certificate -O /tmp/vscode.deb", # not sure about download link
  }

  file { "/tmp/vscode.deb":
    require => Exec["retrieve_vscode_deb"],
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  package { "vscode":
    provider => dpkg,
    ensure   => present,
    source   => "/tmp/vscode.deb"
  }

  file { "/usr/bin/code":
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => 'code-insiders'
  }
}
