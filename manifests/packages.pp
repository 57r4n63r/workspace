class workspace::packages {
  package { 'git':
    ensure => installed,
  }
  package { 'tig':
    ensure => installed,
  }
  package { 'tree':
    ensure => installed,
  }
  package { 'openvpn':
    ensure => installed,
  }
  package { 'arduino':
    ensure => installed,
  }
  package { 'make':
    ensure => installed,
  }
  package { 'curl':
    ensure => installed,
  }
  package { 'composer':
    ensure => installed,
  }
  package { 'docker.io':
    ensure => installed,
  }
  service { 'docker':
    ensure     => 'running',
    enable     => true,
    name       => 'docker',
    hasstatus  => true,
    hasrestart => true,
  }

  package{"qutebrowser":
    ensure => present
  }
}
