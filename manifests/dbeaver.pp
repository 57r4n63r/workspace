class workspace::dbeaver{
  # openjdk-11-jre-headless is required for dbeaver to be installed correctly
  package{"openjdk-11-jre-headless":
    ensure => present
  }

  exec{'retrieve_dbeaver_deb':
    command => "/usr/bin/wget -q https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb -O /tmp/dbeaver.deb",
  }

  file { "/tmp/dbeaver.deb":
    require => Exec["retrieve_dbeaver_deb"],
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  package { "dbeaver":
    provider => dpkg,
    ensure   => present,
    source   => "/tmp/dbeaver.deb"
  }
}
