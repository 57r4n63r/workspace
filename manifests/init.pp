class workspace{
  contain ::workspace::config
  contain ::workspace::packages
  contain ::workspace::apache
  contain ::workspace::vim
  contain ::workspace::mariadb
  contain ::workspace::dbeaver
  contain ::workspace::vscode
  contain ::workspace::tmux
  contain ::workspace::php

  Class['::workspace::config'] ->
  Class['::workspace::packages']->
  Class['::workspace::apache']->
  Class['::workspace::vim']->
  Class['::workspace::mariadb']->
  Class['::workspace::dbeaver']->
  Class['::workspace::vscode']->
  Class['::workspace::tmux']->
  Class['::workspace::php']
}

class {'workspace' : }
