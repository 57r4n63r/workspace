class workspace::mariadb{
  include apt

  apt::key{ 'mariadb':
    id     => '199369E5404BD5FC7D2FE43BCBCB082A1BB943DB',
    server => 'hkp://keyserver.ubuntu.com:80',
  }
  apt::source { 'mariadb':
    location => 'http://sfo1.mirrors.digitalocean.com/mariadb/repo/10.1/ubuntu',
    release  => $::lsbdistcodename,
    repos    => 'main',
    include => {
      src   => false,
      deb   => true,
    },
  }
  class {'::mysql::server':
    package_name     => 'mariadb-server',
    service_name     => 'mysql',
    root_password    => $workspace::config::mysqlRootPassword,
    override_options => {
      mysqld => {
        'log-error' => '/var/log/mysql/mariadb.log',
        'pid-file'  => '/var/run/mysqld/mysqld.pid',
      },
      mysqld_safe => {
        'log-error' => '/var/log/mysql/mariadb.log',
      },
    }
  }

  Apt::Source['mariadb'] ~>
  Class['apt::update'] ->
  Class['::mysql::server']
}
