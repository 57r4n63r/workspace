class workspace::apache
{
  class { 'apache':
    default_vhost => false,
    mpm_module    => 'prefork',
  }

  apache::vhost { 'localhost':
    port    => '80',
    docroot => '/var/www/html',
    docroot_owner => $workspace::config::user,
    docroot_group => 'www-data',
  }

  file { '/var/www/html':
    ensure => 'directory',
    owner  => $workspace::config::user,
    group  => 'www-data',
    mode   => '0775',
  }

  class {'::apache::mod::php':
    package_name => "php7.2",
    path         => "${::apache::params::lib_path}/libphp7.2.so",
  }

  exec{ 'default-permissions':
    command => '/bin/chmod g+s /var/www/html && /usr/bin/setfacl -d -m g::rwx /var/www/html && /usr/bin/setfacl -d -m o::rx /var/www/html'
  }
}
