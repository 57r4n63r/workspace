class workspace::vim{
  package { "vim":
    ensure => present
  }
  file { "/home/$workspace::config::user/.vim":
    ensure => 'directory',
    owner  => $workspace::config::user,
    group  => $workspace::config::user,
    mode   => '0750',
  }

  file { "/home/$workspace::config::user/.vim/bundle":
    ensure => 'directory',
    owner  => $workspace::config::user,
    group  => $workspace::config::user,
    mode   => '0750',
  }

  vcsrepo { "/home/$workspace::config::user/$workspace::config::packageFolder/vimrc":
    ensure   => present,
    provider => git,
    source   => 'https://gitlab.com/57r4n63r/vimrc.git',
    user     => $workspace::config::user,
  }

  vcsrepo { "/home/$workspace::config::user/.vim/bundle/Vundle.vim":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/VundleVim/Vundle.vim.git',
    user     => $workspace::config::user,
  }

  exec { 'make install':
    cwd     => "/home/$workspace::config::user/$workspace::config::packageFolder/vimrc",
    path    => ['/usr/bin', '/usr/sbin','/bin',],
    user    => $workspace::config::user,
    environment => ["HOME=/home/$workspace::config::user"],
  }

  exec { 'yes "" | vim +PluginInstall +qall':
    path    => ['/usr/bin', '/usr/sbin','/bin',],
    user    => $workspace::config::user,
    environment => ["HOME=/home/$workspace::config::user"],
  }
}
