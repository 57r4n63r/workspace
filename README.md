# Workspace puppet cookbooks

## Install

```
$ make install
```

## Reapply configuration

```
$ make apply
```

\*Tested on ubuntu 18.04 Desktop
